import requests


def get_dashboard_details(value):
    r = requests.get(f'http://127.0.0.1:3000/dashboard')
    data = r.json()
    # print(data)
    return data[0]["SIB"][value]


def get_context(context):
    context.total_incoming_bills = get_dashboard_details("total_incoming_bills")
    context.total_outgoing_bills = get_dashboard_details("total_outgoing_bills")
    context.submitted_cards = get_dashboard_details("submitted_cards")
    context.total_incoming_payment = get_dashboard_details("total_incoming_payment")
    context.total_outgoing_payment = get_dashboard_details("total_outgoing_payment")
    context.status_of_card = get_dashboard_details("status_of_card")
    context.total_number_of_cards = get_dashboard_details("total_number_of_cards")
    context.closed_status_of_card = get_dashboard_details("closed_status_of_card")
    context.total_salary_structure = get_dashboard_details("total_salary_structure")

    return context

