# Copyright (c) 2022, shivani and contributors
# For license information, please see license.txt
from __future__ import unicode_literals
import json
import frappe
from frappe.model.document import Document

class random(Document):
	pass

class StudentAccountRequest(Document):
	pass

@frappe.whitelist()
def create_sso_account(doc: str):
	doc_dict = json.loads(doc)
	print('doc=%s' % (doc_dict,))
	student_account_request = frappe.get_doc('Student Account Request', doc_dict['name'])
	print('student_account_request=%s' % (student_account_request))
	return {'message': 'Hello!!!!', 'status': 'oh yes'}
