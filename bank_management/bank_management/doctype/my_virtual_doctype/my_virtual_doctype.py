# Copyright (c) 2022, shivani and contributors
# For license information, please see license.txt

import frappe
from frappe.model.document import Document

DATA = [
    {"name": "rec1", "note": "Note for 1st record"},
    {"name": "rec2", "note": "Note for 2nd record"},
    {"name": "rec3", "note": "Note for 3rd record"},
]


class MyVirtualDoctype(Document):

    def load_from_db(self):
        if self.name != self.doctype:
            d = list(filter(lambda l: l["name"] == self.name, DATA))
            super(Document, self).__init__(d[0])

    def get_list(args):
        return DATA

    def db_update(self):
        pass

    def get_count(self, args):
        pass

    def get_stats(self, args):
        pass
