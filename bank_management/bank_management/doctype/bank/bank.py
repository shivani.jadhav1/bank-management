# Copyright (c) 2022, shivani and contributors
# For license information, please see license.txt

import frappe
from frappe.model.document import Document


class bank(Document):
    pass


@frappe.whitelist()
def get_doctype_colnames(doctype):
    meta = frappe.get_meta(doctype)
    names = meta.get("fields")
    oplist = []
    for name in names:
        oplist.append(name.label)
    return oplist


# doc = frappe.get_doc({
#     "doctype": "Nombre",
#     "nombre": "miguel"
# })
#
# doc.insert()
