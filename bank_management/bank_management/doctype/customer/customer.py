import frappe
from frappe.model.document import Document


class customer(Document):
    full_name: str
    customer_id: str
    enabled: bool

    def set_customer_id(self):
        max_id = frappe.db.get_all('customer', fields=['max(customer_id) as max'])
        if max_id is None:
            self.customer_id = str(1)
        else:
            self.customer_id = str(int(max_id[0]['max']) + 1)

    def before_insert(self):
        self.set_customer_id()
        self.full_name = f'{self.first_name} {self.last_name or ""}'

    def before_save(self):
        if self.enabled:
            details = get_customer_details(self.customer_id)
            doc2 = frappe.get_doc("customer", {"customer_id": self.customer_id})
            print(doc2)
            data = details[0]
            print(data)
            doc = set_customer(doc2, data)
            doc.save()
            self.enabled = False
            self.reload()


def set_customer(doc, api_data):
    all_docfields = set(frappe.db.get_values("customer", {'customer_id': api_data['customer_id']}, "*")[0].keys())
    api_all_fields = list(api_data.keys())
    for field in api_all_fields:
        if field in all_docfields:
            doc.set(field, api_data[field])
        else:
            inside_fields = api_data[field].keys()
            for inner_field in inside_fields:
                if inner_field in all_docfields:
                    doc.set(inner_field, api_data[field][inner_field])

    return doc


# def set_customer(doc, api_data):
#     all_docfields = set(frappe.db.get_values("customer", {'customer_id': api_data['customer_id']}, "*")[0].keys())
#     api_all_fields = list(api_data.keys())
#     for field in api_all_fields:
#         print("field", field)
#
#
#        ield, api_data[field])
#         else:
#             inside_fields = api_data[field].keys()
#             print("inside_fields", inside_fields)
#
#             for inner_field in inside_fields:
#                 print("inner_field", inner_field, "/n")
#
#                 if inner_field in all_docfields:
#                     doc.set(inner_field, api_data[field][inner_field])
#
#     return doc


if __name__ == '__main__':
    def get_customer_details(customer_id: str):

        import requests
        r = requests.get(f'http://127.0.0.1:5000/customers/{customer_id}')
        return r.json()




