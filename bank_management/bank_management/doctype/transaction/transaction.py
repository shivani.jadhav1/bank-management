# Copyright (c) 2022, shivani and contributors
# For license information, please see license.txt

import frappe
import json
from frappe.model.document import Document


class Transaction(Document):
    update_me: bool

    def on_update(self):
        if self.update_me == 1:
            transaction(self.name)
            self.reload()


def transaction(name):
    # open update.json and set value_from_update_file
    with open(
            '/Users/shivanijadhav/work/frappe/frappe-bench/apps/bank_management/bank_management/bank_management'
            '/doctype/transaction/update.json',
            "r") as read_file:
        data = json.load(read_file)
        value = data["amount"]

    frappe.db.set_value('Transaction', name, 'amount', value)
