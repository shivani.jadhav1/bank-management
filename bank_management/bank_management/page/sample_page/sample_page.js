frappe.pages['sample-page'].on_page_load = function(wrapper) {
new MyPage(wrapper);

}
//page content
MyPage = Class.extend({
    init: function(wrapper){
       this.page = frappe.ui.make_app_page({
		parent: wrapper,
		title: 'Sample Home',
		single_column: true
	});
	//make page
        this.make();
    },
    //make page
    make: function(){
    //grab the class
    let me= $(this);

    //body content
//    let body=`<h1>Hello,World!</h1>`;
    //push dom element to page
    $(frappe.render_template(frappe.sample_app_page.body,this)).appendTo(this.page.main)
    }
//end of class
})
    let body = `
			<div class="widget-group ">
				<div class="widget-group-head">

					<div class="widget-group-control"></div>
				</div>
				<div class="widget-group-body grid-col-3"><div class="widget number-widget-box" data-widget-name="Total Incoming Bills">
			<div class="widget-head">
				<div class="widget-label">
					<div class="widget-title"><div class="number-label text-danger">Total Incoming Bills</div></div>
					<div class="widget-subtitle"></div>
				</div>
				<div class="widget-control"><div class="card-actions dropdown pull-right">
				<a data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				...
				</a>
				<ul class="dropdown-menu" style="max-height: 300px; overflow-y: auto;">
					<li class="dropdown-item">
									<a data-action="action-refresh">Refresh</a>
								</li><li class="dropdown-item">
									<a data-action="action-edit">Edit</a>
								</li>
				</ul>
			</div></div>
			</div>
			<div class="widget-body"><div class="widget-content">
				<div class="number" style="color:undefined">50000</div>
				<div class="card-stats grey-stat">
				<span class="percentage-stat-area">

					<span class="percentage-stat">
						0  %
					</span>
				</span>
				<span class="stat-period text-muted">
					since yesterday
				</span>
			</div></div></div>
			<div class="widget-footer"></div>
		</div><div class="widget number-widget-box" data-widget-name="Total Outgoing Bills">
			<div class="widget-head">
				<div class="widget-label">
					<div class="widget-title"><div class="number-label">Total Outgoing Bills</div></div>
					<div class="widget-subtitle"></div>
				</div>
				<div class="widget-control"><div class="card-actions dropdown pull-right">
				<a data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				...
				</a>
				<ul class="dropdown-menu" style="max-height: 300px; overflow-y: auto;">
					<li class="dropdown-item">
									<a data-action="action-refresh">Refresh</a>
								</li><li class="dropdown-item">
									<a data-action="action-edit">Edit</a>
								</li>
				</ul>
			</div></div>
			</div>
			<div class="widget-body"><div class="widget-content">
				<div class="number" style="color:undefined">6</div>
				<div class="card-stats grey-stat">
				<span class="percentage-stat-area">

					<span class="percentage-stat">
						0  %
					</span>
				</span>
				<span class="stat-period text-muted">
					since yesterday
				</span>
			</div></div></div>
			<div class="widget-footer"></div>
		</div><div class="widget number-widget-box" data-widget-name="Total Incoming Payment">
			<div class="widget-head">
				<div class="widget-label">
					<div class="widget-title"><div class="number-label">Total Incoming Payment</div></div>
					<div class="widget-subtitle"></div>
				</div>
				<div class="widget-control"><div class="card-actions dropdown pull-right">
				<a data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				...
				</a>
				<ul class="dropdown-menu" style="max-height: 300px; overflow-y: auto;">
					<li class="dropdown-item">
									<a data-action="action-refresh">Refresh</a>
								</li><li class="dropdown-item">
									<a data-action="action-edit">Edit</a>
								</li>
				</ul>
			</div></div>
			</div>
			<div class="widget-body"><div class="widget-content">
				<div class="number" style="color:undefined">6</div>
				<div class="card-stats grey-stat">
				<span class="percentage-stat-area">

					<span class="percentage-stat">
						0  %
					</span>
				</span>
				<span class="stat-period text-muted">
					since yesterday
				</span>
			</div></div></div>
			<div class="widget-footer"></div>
		</div><div class="widget number-widget-box" data-widget-name="Submitted Cards">
			<div class="widget-head">
				<div class="widget-label">
					<div class="widget-title"><div class="number-label">Submitted Cards</div></div>
					<div class="widget-subtitle"></div>
				</div>
				<div class="widget-control"><div class="card-actions dropdown pull-right">
				<a data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				...
				</a>
				<ul class="dropdown-menu" style="max-height: 300px; overflow-y: auto;">
					<li class="dropdown-item">
									<a data-action="action-refresh">Refresh</a>
								</li><li class="dropdown-item">
									<a data-action="action-edit">Edit</a>
								</li>
				</ul>
			</div></div>
			</div>
			<div class="widget-body"><div class="widget-content">
				<div class="number" style="color:undefined">6</div>
				<div class="card-stats grey-stat">
				<span class="percentage-stat-area">

					<span class="percentage-stat">
						0  %
					</span>
				</span>
				<span class="stat-period text-muted">
					since yesterday
				</span>
			</div></div></div>
			<div class="widget-footer"></div>
		</div><div class="widget number-widget-box" data-widget-name="Total Outgoing Payment-1">
			<div class="widget-head">
				<div class="widget-label">
					<div class="widget-title"><div class="number-label">Total Outgoing Payment</div></div>
					<div class="widget-subtitle"></div>
				</div>
				<div class="widget-control"><div class="card-actions dropdown pull-right">
				<a data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				...
				</a>
				<ul class="dropdown-menu" style="max-height: 300px; overflow-y: auto;">
					<li class="dropdown-item">
									<a data-action="action-refresh">Refresh</a>
								</li><li class="dropdown-item">
									<a data-action="action-edit">Edit</a>
								</li>
				</ul>
			</div></div>
			</div>
			<div class="widget-body"><div class="widget-content">
				<div class="number" style="color:undefined">6</div>
				<div class="card-stats grey-stat">
				<span class="percentage-stat-area">

					<span class="percentage-stat">
						0  %
					</span>
				</span>
				<span class="stat-period text-muted">
					since yesterday
				</span>
			</div></div></div>
			<div class="widget-footer"></div>
		</div><div class="widget number-widget-box" data-widget-name="Status Of">
			<div class="widget-head">
				<div class="widget-label">
					<div class="widget-title"><div class="number-label">Status Of Card</div></div>
					<div class="widget-subtitle"></div>
				</div>
				<div class="widget-control"><div class="card-actions dropdown pull-right">
				<a data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				...
				</a>
				<ul class="dropdown-menu" style="max-height: 300px; overflow-y: auto;">
					<li class="dropdown-item">
									<a data-action="action-refresh">Refresh</a>
								</li><li class="dropdown-item">
									<a data-action="action-edit">Edit</a>
								</li>
				</ul>
			</div></div>
			</div>
			<div class="widget-body"><div class="widget-content">
				<div class="number" style="color:undefined">6</div>
				<div class="card-stats grey-stat">
				<span class="percentage-stat-area">

					<span class="percentage-stat">
						0  %
					</span>
				</span>
				<span class="stat-period text-muted">
					since yesterday
				</span>
			</div></div></div>
			<div class="widget-footer"></div>
		</div><div class="widget number-widget-box" data-widget-name="Total Number Of Cards">
			<div class="widget-head">
				<div class="widget-label">
					<div class="widget-title"><div class="number-label">Total Number Of Cards</div></div>
					<div class="widget-subtitle"></div>
				</div>
				<div class="widget-control"><div class="card-actions dropdown pull-right">
				<a data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				...
				</a>
				<ul class="dropdown-menu" style="max-height: 300px; overflow-y: auto;">
					<li class="dropdown-item">
									<a data-action="action-refresh">Refresh</a>
								</li><li class="dropdown-item">
									<a data-action="action-edit">Edit</a>
								</li>
				</ul>
			</div></div>
			</div>
			<div class="widget-body"><div class="widget-content">
				<div class="number" style="color:undefined">6</div>
				<div class="card-stats grey-stat">
				<span class="percentage-stat-area">

					<span class="percentage-stat">
						0  %
					</span>
				</span>
				<span class="stat-period text-muted">
					since yesterday
				</span>
			</div></div></div>
			<div class="widget-footer"></div>
		</div><div class="widget number-widget-box" data-widget-name="Closed Status Of Card">
			<div class="widget-head">
				<div class="widget-label">
					<div class="widget-title"><div class="number-label">Closed Status Of Card</div></div>
					<div class="widget-subtitle"></div>
				</div>
				<div class="widget-control"><div class="card-actions dropdown pull-right">
				<a data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				...
				</a>
				<ul class="dropdown-menu" style="max-height: 300px; overflow-y: auto;">
					<li class="dropdown-item">
									<a data-action="action-refresh">Refresh</a>
								</li><li class="dropdown-item">
									<a data-action="action-edit">Edit</a>
								</li>
				</ul>
			</div></div>
			</div>
			<div class="widget-body"><div class="widget-content">
				<div class="number" style="color:undefined">6</div>
				<div class="card-stats grey-stat">
				<span class="percentage-stat-area">

					<span class="percentage-stat">
						0  %
					</span>
				</span>
				<span class="stat-period text-muted">
					since yesterday
				</span>
			</div></div></div>
			<div class="widget-footer"></div>
		</div></div>
			</div><div class="widget-group ">
				<div class="widget-group-head">

					<div class="widget-group-control"></div>
				</div>
				<div class="widget-group-body grid-col-2"><div class="widget dashboard-widget-box full-width" data-widget-name="c5da16faca">
			<div class="widget-head">
			<div class="widget-label">
				<div class="widget-title" title="dashboard chart one"><span class="ellipsis" title="dashboard chart one">dashboard chart one</span></div>
					<div class="widget-subtitle"></div>
			</div>
				<div class="widget-control"><div class="chart-actions dropdown pull-right">
			<button data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn btn-xs btn-secondary chart-menu">
				<svg class="icon icon-sm">
					<use xlink:href="#icon-dot-horizontal">
					</use>
				</svg>
			</button>
			<ul class="dropdown-menu dropdown-menu-right">
			<li><a class="dropdown-item" data-action="action-refresh">Refresh</a></li><li><a class="dropdown-item" data-action="action-edit">Edit</a></li><li><a class="dropdown-item" data-action="action-reset">Reset Chart</a></li><li><a class="dropdown-item" data-action="action-list">Customer1 List</a></li>
			</ul>
		</div><div class="chart-actions time-interval-filter btn-group dropdown pull-right">
					<a data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						<button class="btn btn-secondary btn-xs">
			 				<svg class="icon  icon-sm" style="">
			<use class="" href="#icon-calendar"></use>
		</svg>
							<span class="filter-label">Yearly</span>
							<svg class="icon  icon-xs" style="">
			<use class="" href="#icon-select"></use>
		</svg>
						</button>
				</a><ul class="dropdown-menu"><li><a class="dropdown-item">Yearly</a></li><li><a class="dropdown-item">Quarterly</a></li><li><a class="dropdown-item">Monthly</a></li><li><a class="dropdown-item">Weekly</a></li><li><a class="dropdown-item">Daily</a></li></ul></div><div class="chart-actions timespan-filter btn-group dropdown pull-right">
					<a data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						<button class="btn btn-secondary btn-xs">

							<span class="filter-label">Last Year</span>
							<svg class="icon  icon-xs" style="">
			<use class="" href="#icon-select"></use>
		</svg>
						</button>
				</a><ul class="dropdown-menu"><li><a class="dropdown-item">Select Date Range</a></li><li><a class="dropdown-item">Last Year</a></li><li><a class="dropdown-item">Last Quarter</a></li><li><a class="dropdown-item">Last Month</a></li><li><a class="dropdown-item">Last Week</a></li></ul></div><div class="filter-chart btn btn-xs pull-right" data-original-title="" title="">
				<svg class="icon  icon-sm" style="">
			<use class="" href="#icon-filter"></use>
		</svg>
			</div></div>
			</div>
			<div class="widget-body"><div class="chart-loading-state text-muted" style="height: 240px;">Loading...</div><div class="chart-loading-state text-muted" style="height: 240px; display: none;">No Data...
			</div><div></div></div>
			<div class="widget-footer">
			</div>
		</div></div>
			</div>

			<div class="sidenav">
  <a href="#">About</a>
  <a href="#">Services</a>
  <a href="#">Clients</a>
  <a href="#">Contact</a>
</div>

<!-- Page content -->
<div class="main">
  ...
</div>`;
//   frappe.estate_app_page = {}
   frappe.sample_app_page  = {
   body: body
   }





