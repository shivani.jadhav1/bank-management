from __future__ import unicode_literals
import frappe
from frappe import _

def execute(filters=None):
    if not filters:
        filters = {}

    columns = get_columns(filters)
    data = get_data(filters)
    # chart = get_chart_data(data)

    return columns, data, None, chart


def get_columns(filters=None):
    columns = [
        {
            "label": _("Total Incoming Bills"),
            "fieldname": "total_incoming_bills ",
            "fieldtype": "Int",
            "width": 100
        },
        {
            "label": _("Total Outgoing Bills"),
            "fieldname": "total_outgoing_bills",
            "fieldtype": "Int",
            "width": 100
        },
        {
            "label": _("Total Incoming Payment"),
            "fieldname": "total_incoming_payments",
            "fieldtype": "Int",
            "width": 150
        },
        {
            "label": _("Submitted Cards"),
            "fieldname": "submitted_cards",
            "fieldtype": "Int",
            "width": 150
        },
        {
            "label": _("Total Outgoing Payment"),
            "fieldname": "total_outgoing_payment",
            "fieldtype": "Int",
            "width": 150
        },
        {
            "label": _("Status Of Card"),
            "fieldname": "status_of_card",
            "fieldtype": "Int",
            "width": 80
        },
        {
            "label": _("Total Number Of Cards"),
            "fieldname": "total_number_of_cards",
            "fieldtype": "Int",
            "width": 80
        },
        {
            "label": _("Closed Status Of Card"),
            "fieldname": "closed_status_of_card",
            "fieldtype": "Int",
            "width": 80
        }
    ]

    return columns


def get_data(filters=None):
    data = []
    conditions = get_filter_conditions(filters)
    map_results = frappe.db.sql("""
			SELECT total_incoming_bills , total_outgoing_bills ,total_incoming_payments ,submitted_cards ,total_outgoing_payment,
			  status_of_card,total_number_of_cards,closed_status_of_card
			FROM `tabApi_Dashboard`
			WHERE
					docstatus = 0 %s""" % (conditions), as_dict=1)

    # for test in map_results:
    #     data.append({
    #         'student': test.student,
    #         'discipline': test.discipline,
    #         'student_name': test.student_name,
    #         'program': test.program,
    #         'test_rit_score': test.test_rit_score,
    #         'test_percentile': test.test_percentile,
    #         'test_date': test.test_date,
    #         'academic_term': test.academic_term
    #     })
    #
    # return data


# def get_chart_data(data):
#     if not data:
#         return None
#
#     labels = []
#     math = []
#     reading = []
#     language = []
#
#     for entry in data:
#         if entry.get("academic_term") not in labels:
#             labels.append(entry.get("academic_term"))
#         if entry.get("discipline") == "Mathematics":
#             math.append(entry.get("test_percentile"))
#         if entry.get("discipline") == "Reading":
#             reading.append(entry.get("test_percentile"))
#         if entry.get("discipline") == "Language":
#             language.append(entry.get("test_percentile"))
#
#     return {
#         "data": {
#             "labels": labels,
#             "datasets": [
#                 {
#                     "name": _("Math Percentile"),
#                     "values": math
#                 },
#                 {
#                     "name": _("Reading Percentile"),
#                     "values": reading
#                 },
#                 {
#                     "name": _("Language Percentile"),
#                     "values": language
#                 }
#             ]
#         },
#         "type": "line"
#
#     }
#
#
# def get_filter_conditions(filters):
#     conditions = ""
#
#     if filters.get("discipline"):
#         conditions += " and discipline = '%s' " % (filters.get("discipline"))
#
#     if filters.get("student"):
#         conditions += " and student = '%s' " % (filters.get("student"))
#
#     return conditions
