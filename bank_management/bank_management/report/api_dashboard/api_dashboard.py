# Copyright (c) 2022, shivani and contributors
# For license information, please see license.txt

# Copyright (c) 2022, shivani and contributors
# For license information, please see license.txt
from __future__ import unicode_literals

import frappe


@frappe.whitelist()
def get_dashboard_details(value):
    import requests
    r = requests.get(f'http://127.0.0.1:3000/dashboard')
    data = r.json()
    # print(data)
    return data[0]["dashboard_values"][value]


def query():
    return [
        {"xAxisField": "Total Incoming Bills", "yAxisField": get_dashboard_details("total_incoming_bills")},
        {"xAxisField": "Total Outgoing Bills", "yAxisField": 9},
        {"xAxisField": "Total Outgoing Payment", "yAxisField": 6},
        {"xAxisField": "Status Of Card", "yAxisField": 6},
        {"xAxisField": "Total Number Of Card", "yAxisField": 6},
        {"xAxisField": "Closed Status Of Card", "yAxisField": 6}

    ]


def get_columns():
    return [
        {
            "fieldname": "xAxisField",
            "fieldtype": "Str",
            "label": "X-Axis",
            "width": 100
        },
        {
            "fieldname": "yAxisField",
            "fieldtype": "Int",
            "label": "Y-Axis",
            "width": 100
        },
    ]


def get_data(data, fltr):
    # return [value for value in data if value["xAxisField"] > str(fltr.xAxisField)]
    return [value for value in data]


def execute(filters=None):
    query_result = query()
    columns = get_columns()
    data = get_data(query_result, filters)
    print("tesss", type(data), data,type(data[0]), data[0])

    return columns, data


if __name__ == '__main__':

    print(get_dashboard_details("total_incoming_bills"))

