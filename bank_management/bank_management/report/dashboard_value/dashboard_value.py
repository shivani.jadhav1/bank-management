def execute(filters=None):
    # columns = ["Bills", "Values"]
    columns = [
        {'fieldname': 'letter', 'label': 'Letter', 'fieldtype': 'Data', 'align': 'right', 'width': 200},
        {'fieldname': 'number', 'label': 'Number', 'fieldtype': 'Int', 'align': 'right', 'width': 200}
    ]
    data = [['total_incoming_bills', 2], ['total_outgoing_bills', 2], ['total_incoming_payments', 8], ['submitted_cards', 7], ['total_outgoing_payment',8],
            ['status_of_card',8],['total_number_of_cards',7],['closed_status_of_card',3]]
    data = [
        {'letter': 'c', 'number': 2, 'indent': 0},
        {'letter': 'a', 'number': 2, 'indent': 1},
        {'letter': 't', 'number': 8, 'indent': 2},
        {'letter': 's', 'number': 7, 'indent': 0}
    ]
    message = ["The equivalent of the letters 'cats' in numbers is '2287'"]
    chart = {'data': {'labels': ['d', 'o', 'g', 's'], 'datasets': [{'values': [3, 6, 4, 7]}]}, 'type': 'bar'}
    report_summary = [
        {"label": "bills", "value": 315, 'indicator': 'Red'},
        {"label": "values", "value": 654, 'indicator': 'Blue'}
    ]
    message = [
        "The letters '<b>cats</b>' in numbers is <span style='color:Red;'>315</span>",
        "<br>",
        "The letters '<b>dogs</b>' in numbers is <span style='color:Blue;'>654</span>"
    ]
    chart = {
        'data': {
            'labels': ['b', 'i', 'l', 'l', 's'],
            'datasets': [
                {'name': 'Bills', 'values': [3, 6, 4, 7, 5], 'chartType':'bar'},
                {'name': 'Values', 'values': [5, 7, 8, 10, 12], 'chartType':'line'}
            ]
        },
        'type': 'axis-mixed'
    }
    return columns, data, None, chart, report_summary, message
