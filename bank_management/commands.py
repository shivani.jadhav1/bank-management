import subprocess

import click


@click.command('stop')
@click.argument('port')
def stop_bench(port):
    port = int(port)
    awk_column = "awk '{print $2}'"
    subprocess.run(f"lsof -i tcp:{port} | grep -v PID | {awk_column} | xargs kill", shell=True)
    subprocess.run(f"lsof -i tcp:{port + 1000} | grep -v PID | {awk_column} | xargs kill", shell=True)
    subprocess.run(f"lsof -i tcp:{port + 3000} | grep -v PID | {awk_column} | xargs kill", shell=True)
    subprocess.run(f"lsof -i tcp:{port + 4000} | grep -v PID | {awk_column} | xargs kill", shell=True)
    subprocess.run(f"lsof -i tcp:{port + 5000} | grep -v PID | {awk_column} | xargs kill", shell=True)
    subprocess.run(f"ps aux | grep bench | {awk_column} | xargs kill", shell=True)


commands = [
    stop_bench
]